package app

import (
	"fmt"
	"junior/internal/app/endpoint"
	"junior/internal/app/middleware"
	"junior/internal/app/service"
	"log"

	"github.com/labstack/echo/v4"
)

type App struct {
	e *endpoint.Endpoint
	s *service.Service
	echo *echo.Echo
}

func New() (*App, error) {
	a := &App{}

	a.s = service.New()
	a.e = endpoint.New(a.s)

	a.echo = echo.New()

	// a.echo.Use(middleware.Logger())
  	// a.echo.Use(middleware.Recover())
	a.echo.Use(middleware.CheckAdminRoleMIddleware)

	a.echo.GET("/answer", a.e.Answer)

	return a, nil
}

func (a *App) Run() error {
	fmt.Println("Server started")
	
	err := a.echo.Start(":8088")
	if err != nil {
		log.Fatal(err)
	}

	return nil
}