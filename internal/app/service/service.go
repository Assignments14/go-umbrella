package service

import "time"

type Service struct{}

func New() *Service {
	return &Service{}
}

func (s *Service) DaysUntil() int64 {
	deadLine := time.Date(2025, time.January, 1, 0, 0, 0, 0, time.UTC)
	dur := time.Until(deadLine)
	return int64(dur.Hours())/24
}
