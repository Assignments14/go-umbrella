package endpoint

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Service interface {
	DaysUntil() int64
}

type Endpoint struct {
	service Service
}

func New(s Service) *Endpoint {
	return &Endpoint{
		service: s,
	}
}

func (e *Endpoint) Answer(ctx echo.Context) error {
	answer := fmt.Sprintf("Days till deadline: %d", e.service.DaysUntil())
	err := ctx.String(http.StatusOK, answer)
	if err != nil {
		return err
	}
	return nil
}