package middleware

import (
	"strings"
	"log/slog"

	"github.com/labstack/echo/v4"
)

const (
	RED_BUTTON_DETECTED = "red button user detected"
	ADMIN_ROLE          = "admin"
	USER_ROLE_HEADER    = "User-Role"
)

func CheckAdminRoleMIddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		userRoleHeader := c.Request().Header.Get(USER_ROLE_HEADER)

		if strings.Contains(userRoleHeader, ADMIN_ROLE) {
			slog.Info(RED_BUTTON_DETECTED)
		}

		err := next(c)
		if err != nil {
			return err
		}

		return nil
	}
}